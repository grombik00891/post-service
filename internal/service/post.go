package service

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"post-service/internal/app/config"
	"post-service/internal/entity"
	"strings"
	"sync"
)

type PostService struct {
	PostRepo    entity.IPostRepository
	PostGetaway entity.IPostGetaway
	Cfg         *config.Config
}

func NewPostService(cfg *config.Config, postRepo entity.IPostRepository, postGetaway entity.IPostGetaway) entity.IPostService {
	PostService := new(PostService)
	PostService.PostRepo = postRepo
	PostService.PostGetaway = postGetaway
	PostService.Cfg = cfg
	return PostService
}

func (s PostService) GetCommentStatistic(ctx context.Context, postId int) ([]entity.CommentStatistic, error) {
	return s.PostRepo.SelectCommentStatistic(ctx, postId)
}

type commentsInfo struct {
	Comments     string
	CommentsHash string
}

func (s PostService) UpdateCommentStatistic(ctx context.Context) error {

	comments, err := s.PostGetaway.GetAllComments(ctx)
	if err != nil {
		return err
	}

	currentPostsMap := make(map[int]commentsInfo)
	for _, comment := range comments {
		if currentPost, ok := currentPostsMap[comment.PostId]; ok {
			currentPost.Comments += " " + comment.Body
			currentPostsMap[comment.PostId] = currentPost
		} else {
			currentPostsMap[comment.PostId] = commentsInfo{Comments: comment.Body}
		}
	}

	for postId, currentPost := range currentPostsMap {
		hash := sha1.Sum([]byte(currentPostsMap[postId].Comments))
		currentPost.CommentsHash = hex.EncodeToString(hash[:])
		currentPostsMap[postId] = currentPost
	}

	dbPosts, err := s.PostRepo.SelectPosts(ctx)
	if err != nil {
		return err
	}

	for _, post := range dbPosts {
		if currentPostsMap[post.Id].CommentsHash == post.Hash {
			delete(currentPostsMap, post.Id)
		}
	}

	var wg sync.WaitGroup
	errChan := make(chan error, len(currentPostsMap))
	for postId, commInfo := range currentPostsMap {

		wg.Add(1)
		go func(postId int, commentsInfo commentsInfo, errChan chan error) {
			// Считаем
			commentsInfo.Comments = strings.ReplaceAll(commentsInfo.Comments, "\n", " ")
			wordMap := make(map[string]int)
			var commentStatistic []entity.CommentStatistic

			for _, word := range strings.Split(commentsInfo.Comments, " ") {
				wordMap[word]++
			}

			for word, count := range wordMap {
				commentStatistic = append(commentStatistic, entity.CommentStatistic{
					PostId: postId,
					Word:   word,
					Count:  count,
				})
			}

			// Проверяем существование поста
			isPostAvailable, err := s.PostRepo.CheckPost(ctx, postId)
			if err != nil {
				errChan <- err
				return
			}

			tx, err := s.PostRepo.GetTx(ctx)
			if err != nil {
				errChan <- err
				return
			}
			defer tx.Rollback(ctx)

			// Удаляем/Изменяем пост/статистику
			if !isPostAvailable {
				err := s.PostRepo.TxInsertPost(ctx, tx, &entity.Post{Id: postId, Hash: currentPostsMap[postId].CommentsHash})
				if err != nil {
					errChan <- err
					return
				}
			} else {
				err := s.PostRepo.TxUpdatePost(ctx, tx, &entity.Post{Id: postId, Hash: currentPostsMap[postId].CommentsHash})
				if err != nil {
					errChan <- err
					return
				}

				err = s.PostRepo.TxDeleteCommentStatistics(ctx, tx, postId)
				if err != nil {
					errChan <- err
					return
				}
			}

			// Вставляем статистику
			err = s.PostRepo.TxInsertCommentStatistics(ctx, tx, commentStatistic)
			if err != nil {
				errChan <- err
				return
			}

			err = tx.Commit(ctx)
			if err != nil {
				errChan <- err
				return
			}
			wg.Done()
		}(postId, commInfo, errChan)
	}

	wg.Wait()
	close(errChan)

	return <-errChan
}
