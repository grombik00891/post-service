package handler

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"go.uber.org/zap"
	"net/http"
	routerMiddleware "post-service/internal/api/middleware"
	"post-service/internal/entity"
	"strconv"
	"time"
)

type postHandler struct {
	PostService entity.IPostService
	Logger      *zap.Logger
}

func RegisterPostHandlers(r *chi.Mux, service entity.IPostService, logger *zap.Logger, routerMiddleware routerMiddleware.IMiddleware) {
	PostHandler := new(postHandler)
	PostHandler.PostService = service
	PostHandler.Logger = logger

	r.Route("/post", func(r chi.Router) {
		r.Use(routerMiddleware.PanicRecovery)
		r.Use(middleware.Timeout(time.Second * 10))
		r.Use(middleware.RequestID)
		r.Use(routerMiddleware.ContentTypeJSON)

		r.Get("/{postId}/comments/statistics", routerMiddleware.DebugLogger(PostHandler.GetCommentStatistic))
	})
}

func (h postHandler) GetCommentStatistic(w http.ResponseWriter, r *http.Request) ([]byte, int) {

	postId, err := strconv.Atoi(chi.URLParam(r, "postId"))

	if err != nil {
		resp, code := entity.HandleError(r.Context(), h.Logger, entity.NewLogicError(err, "bad request", http.StatusBadRequest))
		w.WriteHeader(code)
		w.Write(resp)
		return resp, code
	}

	commentStatistic, err := h.PostService.GetCommentStatistic(r.Context(), postId)
	if commentStatistic == nil {
		err = entity.NotFoundErrorResponse
	}

	if err != nil {
		resp, code := entity.HandleError(r.Context(), h.Logger, err)
		w.WriteHeader(code)
		w.Write(resp)
		return resp, code
	}

	resp, _ := json.Marshal(commentStatistic)

	w.WriteHeader(http.StatusOK)
	w.Write(resp)
	return resp, http.StatusOK
}
