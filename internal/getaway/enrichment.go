package getaway

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"post-service/internal/app/config"
	"post-service/internal/entity"
)

type errorResponse struct {
	Error string `json:"error"`
}

type jsonPlaceHolder struct {
	domain string
	client *http.Client
}

func NewJsonPlaceHolder(cfg *config.Config) entity.IPostGetaway {
	return &jsonPlaceHolder{
		domain: cfg.PostDomain,
		client: &http.Client{Timeout: cfg.PostTimeout},
	}
}

func (e *jsonPlaceHolder) GetAllComments(ctx context.Context) ([]entity.Comment, error) {
	var comments []entity.Comment

	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("https://%s/comments", e.domain), nil)
	if err != nil {
		return nil, err
	}

	resp, err := e.client.Do(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		var errorResponse errorResponse
		err = json.NewDecoder(resp.Body).Decode(&errorResponse)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("age request status code not OK: %d", resp.StatusCode))
		}
		return nil, errors.New(fmt.Sprintf("age request error: %s", errorResponse.Error))
	}

	err = json.NewDecoder(resp.Body).Decode(&comments)
	if err != nil {
		return nil, err
	}

	return comments, nil
}

func (e *jsonPlaceHolder) GetCommentsByPostId(ctx context.Context, postId int) ([]entity.Comment, error) {
	var comments []entity.Comment

	req, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("https://%s/posts/%d/comments", e.domain, postId), nil)
	if err != nil {
		return nil, err
	}

	resp, err := e.client.Do(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		var errorResponse errorResponse
		err = json.NewDecoder(resp.Body).Decode(&errorResponse)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("age request status code not OK: %d", resp.StatusCode))
		}
		return nil, errors.New(fmt.Sprintf("age request error: %s", errorResponse.Error))
	}

	err = json.NewDecoder(resp.Body).Decode(&comments)
	if err != nil {
		return nil, err
	}

	return comments, nil
}
