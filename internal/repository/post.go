package repository

import (
	"context"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"post-service/internal/entity"
)

type PostRepository struct {
	db *pgxpool.Pool
}

func NewPostRepository(db *pgxpool.Pool) entity.IPostRepository {
	PostRepository := new(PostRepository)
	PostRepository.db = db
	return PostRepository
}

func (r PostRepository) GetTx(ctx context.Context) (pgx.Tx, error) {
	tx, err := r.db.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func (r PostRepository) SelectCommentStatistic(ctx context.Context, postId int) ([]entity.CommentStatistic, error) {
	var commentsStatistic []entity.CommentStatistic

	rows, err := r.db.Query(ctx, "SELECT post_id, word, count FROM comment_statistic WHERE post_id = $1 ORDER BY count DESC", postId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var commentStatistic entity.CommentStatistic
		err = rows.Scan(
			&commentStatistic.PostId,
			&commentStatistic.Word,
			&commentStatistic.Count,
		)
		if err != nil {
			return nil, err
		}

		commentsStatistic = append(commentsStatistic, commentStatistic)
	}

	return commentsStatistic, nil
}

func (r PostRepository) SelectPosts(ctx context.Context) ([]entity.Post, error) {
	var posts []entity.Post

	rows, err := r.db.Query(ctx, "SELECT id, hash FROM post")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var post entity.Post
		err = rows.Scan(
			&post.Id,
			&post.Hash,
		)
		if err != nil {
			return nil, err
		}

		posts = append(posts, post)
	}

	return posts, nil
}

func (r PostRepository) CheckPost(ctx context.Context, postId int) (bool, error) {

	rows, err := r.db.Query(ctx, "SELECT * FROM post WHERE id = $1", postId)
	if err != nil {
		return false, err
	}
	defer rows.Close()

	return rows.Next(), nil
}

func (r PostRepository) TxInsertPost(ctx context.Context, tx pgx.Tx, post *entity.Post) error {

	_, err := tx.Exec(ctx, "INSERT INTO post (id, hash) VALUES ($1, $2)", post.Id, post.Hash)
	if err != nil {
		return err
	}

	return nil
}

func (r PostRepository) TxUpdatePost(ctx context.Context, tx pgx.Tx, post *entity.Post) error {

	_, err := tx.Exec(ctx, "UPDATE post SET hash = $2 WHERE id = $1", post.Id, post.Hash)
	if err != nil {
		return err
	}

	return nil
}

func (r PostRepository) TxDeleteCommentStatistics(ctx context.Context, tx pgx.Tx, postId int) error {

	_, err := tx.Exec(ctx, "DELETE FROM comment_statistic WHERE post_id = $1", postId)
	if err != nil {
		return err
	}

	return nil
}

func (r PostRepository) TxInsertCommentStatistics(ctx context.Context, tx pgx.Tx, commentStatistics []entity.CommentStatistic) error {

	rows := make([][]interface{}, 0, len(commentStatistics))

	for _, item := range commentStatistics {
		rows = append(rows, []interface{}{item.PostId, item.Word, item.Count})
	}

	_, err := tx.CopyFrom(ctx, pgx.Identifier{"comment_statistic"}, []string{"post_id", "word", "count"}, pgx.CopyFromRows(rows))
	if err != nil {
		return err
	}

	return nil
}
