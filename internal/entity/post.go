package entity

import (
	"context"
	"github.com/jackc/pgx/v5"
)

type IPostService interface {
	GetCommentStatistic(ctx context.Context, postId int) ([]CommentStatistic, error)
	UpdateCommentStatistic(ctx context.Context) error
}

type IPostRepository interface {
	SelectCommentStatistic(ctx context.Context, postId int) ([]CommentStatistic, error)
	SelectPosts(ctx context.Context) ([]Post, error)
	CheckPost(ctx context.Context, postId int) (bool, error)
	GetTx(ctx context.Context) (pgx.Tx, error)
	TxInsertPost(ctx context.Context, tx pgx.Tx, post *Post) error
	TxUpdatePost(ctx context.Context, tx pgx.Tx, post *Post) error
	TxDeleteCommentStatistics(ctx context.Context, tx pgx.Tx, postId int) error
	TxInsertCommentStatistics(ctx context.Context, tx pgx.Tx, commentStatistics []CommentStatistic) error
}

type IPostGetaway interface {
	GetAllComments(ctx context.Context) ([]Comment, error)
	GetCommentsByPostId(ctx context.Context, postId int) ([]Comment, error)
}

type Post struct {
	Id   int
	Hash string
}

type Comment struct {
	PostId int    `yaml:"postId"`
	Body   string `json:"body"`
}

type CommentStatistic struct {
	PostId int
	Word   string
	Count  int
}
