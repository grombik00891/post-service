-- +goose Up

--
-- таблица `post`
--

CREATE TABLE post (
                          id INT NOT NULL,
                          hash text NOT NULL
);

--
-- таблица `comment_statistic`
--

CREATE TABLE comment_statistic (
                          post_id INT NOT NULL,
                          word text NOT NULL,
                          count int NOT NULL
);